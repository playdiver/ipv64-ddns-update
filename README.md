# ipv64-ddns-update 

Dieses Skript ist so gebaut, dass jede Minute geprüft wird, ob die IP des Internetanschlusses der hinterlegten IP der Domain entspricht. Wenn dies nicht der Fall ist, wird die IP übermittelt.
Das Skript kann mit IPv4 und IPv6 umgehen. Es gibt einen Einrichtungsassistenten, der folgendermaßen aufgerufen werden kann: ./ipv64-ddns-update -set 
Bitte bei den Wahrheitswerten "True" und "False" genauso mit Anführungszeichen und Groß-/Kleinschreibung eintragen wie gerade beschrieben. Das Skript stellt Dir dann die passenden Fragen.

ToDo:
- [x] Systemd Service entwickeln
- [x] Log Datei einbauen
- [x] für die Hauptplattform Deb Pakete bereitstellen (arm, AMD64, X86) und ein passendes apt Repo


# Inbetriebnahme (für alle Debian & Ubuntu Versionen)
```bash
sudo apt-get install gnupg -y
wget -O - http://repo.ipv64.net/debian/stable/repo_key.key | sudo gpg --dearmor -o /usr/share/keyrings/ipv64-archive-keyring.gpg
sudo echo "deb [signed-by=/usr/share/keyrings/ipv64-archive-keyring.gpg] http://repo.ipv64.net/debian/stable ipv64_stable main" > /etc/apt/sources.list.d/ipv64.list
sudo apt update
sudo apt install ipv64-ipv64-ddns-update
ipv64-ddns-update --check
```

Wenn bis zum letzten Befehl alles ohne Fehler durchgelaufen ist:
```bash
systemctl daemon-reload
systemctl enable ipv64-ddns-update.service
systemctl restart ipv64-ddns-update.service
```

Nun wird der Service bei jedem Start mitgestartet.

# Inbetriebnahme (für alle anderen OS)
```bash
mkdir -m 777 -p /var/log/ipv64-ddns-update
chown -R root:root /var/log/ipv64-ddns-update
git clone git@gitlab.com:playdiver/ipv64-ddns-update.git ~/ipv64-ddns-update
cd ~/ipv64-ddns-update
chmod +x ./ipv64-ddns-update
./ipv64-ddns-update --check
./ipv64-ddns-update
```
Für den Dauerbetrieb empfehle ich Folgendes:
```bash
sudo crontab -e
```
Und in der letzte Zeile Folgendes eintragen:
```crontab
@reboot /script/path
```

# Ablauf Paket Veröffentlichung / Qualitätsmanagement
```mermaid
graph
A(Entwicklung der neuen Version / Verbesserungen)-->B(Anpassungen werden lokal getestet)
B-->C(Wenn erfolgreich, wird das Changelog aktualisiert)
C-->D(git commit wird gebaut)
D-->E(Commit wird zum GIT-Repo übermittelt)
E-->F(Pipeline wird angestoßen)--automatische Tests werden durchgeführt-->G{Auswertung der Tests}
G--wenn fehlerlos-->H(Merge in den Master Branch)
H-->J(Bauen des Debian / Ubuntu Paket)
J-->K(Paket wird auf Testsystem installiert und getestet)
K--wenn fehlerlos-->L(wird das Paket auf den APT-REPO-Server geschoben)
L-->N(Paket wird verfügbar gemacht)

G--wenn fehlerhaft-->A
K--wenn fehlerhaft-->A
```

# Danksagung

Ich möchte mich für das Projekt ipv64.net bedanken. Danke an Dennis Schröder (Projektseite https://ipv64.net) für den nicen Dyn-DNS Dienst.
<br>
<br>
Ich danke aber auch der **rennschnecke500** für die Rechtschreibung.

# Disclaimer

Das Skript benutzt die API von ipv64.net, hat aber keine wirtschaftliche Verbindung zu diesem Projekt. Dieses Skript wurde von einem Zuschauer von Dennis Schröder erstellt.

